<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 31/07/2017
 * Time: 23:03
 */

namespace App\Models;

use SON\Model\Table;

class Client extends Table
{
    protected $table = "clients";
}