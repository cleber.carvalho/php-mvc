<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 27/06/2017
 * Time: 22:35
 */

namespace App\Controllers;

use App\Conn;
use SON\Controller\Action;
use SON\DI\Container;

class IndexController extends Action
{
	

    public function  index(){
    	$client = Container::getModel("Client");
    	$this->view->clients = $client->fechAll();
    	$this->render("index");
    }

    public function  contact(){
        $client = Container::getModel("Client");
        $this->view->clients = $client->find(2);
        $this->render("contact");
    }


}