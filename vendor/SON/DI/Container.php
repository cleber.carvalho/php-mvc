<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 31/07/2017
 * Time: 23:21
 */

namespace SON\DI;

use App\Conn;

class Container
{
    public static function getModel($model){
        $class = "\\App\\Models\\".ucfirst($model);
        return new $class(Conn::getDb());
    }
}